## What is it

It's an example of usage [FrontBlocksFramework](https://gitlab.com/lightsource/front-blocks-framework) with [Webpack](https://www.npmjs.com/package/webpack) to build and minify scss/js files
(using [Laravel Mix](https://www.npmjs.com/package/laravel-mix) as a wrapper)

## How to use

1. Main files - **example.php** and **node-js/webpack.mix.js**
2. You can view the files within github or clone the repository (and install composer/node packages) and then have a look and test locally.  

Also the screenshot of the example.php output is attached.
