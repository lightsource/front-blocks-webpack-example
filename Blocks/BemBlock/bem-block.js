import bemBlock from '../../node-js/node_modules/@lightsource/bem-block';

(function () {

    bemBlock.settings.ERROR_CALLBACK = (error) => {
        console.log('Something wrong with bemBlock', error);
    };

    let exportData = bemBlock;

    window.example = window.example || {};
    window.example.bemBlock = exportData;

}());
