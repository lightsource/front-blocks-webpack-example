<?php

namespace LightSource\FrontBlocksExample\Header;

use LightSource\FrontBlocksFramework\Controller;

class HeaderC extends Controller
{

    public function getModel(): ?Header
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return parent::getModel();
    }

}
