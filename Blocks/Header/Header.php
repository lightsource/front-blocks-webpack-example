<?php

namespace LightSource\FrontBlocksExample\Header;

use LightSource\FrontBlocksFramework\Model;

class Header extends Model {

	protected string $name;

	public function loadByTest() {

		parent::load();
		$this->name = 'I\'m Header';

	}

}
