<?php

namespace LightSource\FrontBlocksExample\Article;

use LightSource\FrontBlocksExample\Button\ButtonC;
use LightSource\FrontBlocksFramework\Controller;

class ArticleC extends Controller
{

    protected ButtonC $button;

    public function getModel(): ?Article
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return parent::getModel();
    }

}
