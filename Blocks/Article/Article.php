<?php

namespace LightSource\FrontBlocksExample\Article;

use LightSource\FrontBlocksExample\Button\Button;
use LightSource\FrontBlocksFramework\Model;

class Article extends Model
{

    protected string $name;
    protected Button $button;

    public function loadByTest()
    {
        parent::load();
        $this->name = 'I\'m Article, I contain another block';
        $this->button->loadByTest();
    }

}
