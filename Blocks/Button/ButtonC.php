<?php

namespace LightSource\FrontBlocksExample\Button;

use LightSource\FrontBlocksExample\BemBlock\BemBlockC;
use LightSource\FrontBlocksFramework\Controller;

class ButtonC extends Controller
{

    protected BemBlockC $bemBlock;

    public function getModel(): ?Button
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return parent::getModel();
    }

}
