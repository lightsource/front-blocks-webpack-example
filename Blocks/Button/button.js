(function () {

    let bemBlock = window.example.bemBlock || null;

    if (!bemBlock) {
        throw new Error("Required dependency is missing");
    }

    const BUTTON = {
        "class": {
            "ELEMENT": "button",
            "MESSAGE": "button__message",
        }
    };

    class Button extends bemBlock.Class {

        constructor(element) {

            super(element);

            this._init();

        }

        _init() {

            this.getElement().querySelector("." + BUTTON.class.MESSAGE).textContent = "(dynamic js string - block's code has an access to an external dependency)";
        }

    }

    bemBlock.Register("." + BUTTON.class.ELEMENT, Button);

}());