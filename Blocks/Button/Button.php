<?php

namespace LightSource\FrontBlocksExample\Button;

use LightSource\FrontBlocksFramework\Model;

class Button extends Model {

	protected string $name;

	public function loadByTest() {

		parent::load();
		$this->name = 'I\'m Button';

	}

}
